"use strict";
exports.id = 129;
exports.ids = [129];
exports.modules = {

/***/ 483:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _mui_material_styles__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8442);
/* harmony import */ var _mui_material_styles__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_mui_material_styles__WEBPACK_IMPORTED_MODULE_0__);

// Create a theme instance.
const theme = (0,_mui_material_styles__WEBPACK_IMPORTED_MODULE_0__.responsiveFontSizes)((0,_mui_material_styles__WEBPACK_IMPORTED_MODULE_0__.createTheme)({
    palette: {
        primary: {
            main: "#41464C",
            contrastText: "#FFFFFF"
        },
        secondary: {
            main: "#fff",
            contrastText: "#41464C"
        }
    },
    typography: {
        fontFamily: "'AppleGothic','NanumSquare', sans-serif",
        h1: {
            fontFamily: "'AppleGothic','NanumSquare', sans-serif",
            color: "white",
            fontSize: 95,
            fontWeight: "bold"
        },
        h2: {
            fontFamily: "'AppleGothic','NanumSquare', sans-serif",
            fontSize: 50,
            fontWeight: "bold"
        },
        h3: {
            fontFamily: "'AppleGothic','NanumSquare', sans-serif",
            fontSize: 30
        },
        h4: {
            fontFamily: "'AppleGothic','NanumSquare', sans-serif",
            fontSize: 25,
            fontWeight: 600,
            color: "#595757"
        },
        h5: {
            fontFamily: "'AppleGothic','NanumSquare', sans-serif",
            fontWeight: 600,
            fontSize: 20,
            color: "#2976bc"
        },
        body1: {
            fontFamily: "'AppleGothic','NanumSquare', sans-serif",
            fontSize: 18,
            fontWeight: 600
        },
        body2: {
            fontFamily: "'AppleGothic','NanumSquare', sans-serif",
            fontSize: 18
        }
    },
    components: {
        MuiTypography: {
            defaultProps: {
                variantMapping: {
                    h2: "h2",
                    h3: "h1",
                    h4: "h1",
                    h5: "h1",
                    h6: "h2",
                    subtitle1: "h3",
                    subtitle2: "h3",
                    body2: "body2",
                    body1: "body1"
                }
            }
        },
        MuiCssBaseline: {
            styleOverrides: `
        * {
          box-sizing: border-box;
          margin: 0;
          padding: 0;
        }
        a {
          text-decoration: none;
          color: inherit;
        }
        ul {
          list-style: none;
          padding-left: 0,
        }
        @font-face {
          font-family: 'AppleGothic';
          src: url('/font/AppleSDGothicNeoM.ttf') format('opentype');
          font-weight: normal;
          font-style: normal;
        }
        @font-face {
          font-family: 'AppleGothic';
          src: url('/font/AppleSDGothicNeoEB.ttf') format('opentype');
          font-weight:bold;
        }
        @font-face {
          font-family: 'AppleGothic';
          src: url('/font/AppleSDGothicNeoB.ttf') format('opentype');
          font-weight:600;
        }
      `
        }
    }
}));
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (theme);


/***/ }),

/***/ 5485:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ createEmotionCache)
/* harmony export */ });
/* harmony import */ var _emotion_cache__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1913);
/* harmony import */ var _emotion_cache__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_emotion_cache__WEBPACK_IMPORTED_MODULE_0__);

function createEmotionCache() {
    return _emotion_cache__WEBPACK_IMPORTED_MODULE_0___default()({
        key: "css",
        prepend: true
    });
};


/***/ })

};
;