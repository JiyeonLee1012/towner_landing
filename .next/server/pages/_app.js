"use strict";
(() => {
var exports = {};
exports.id = 888;
exports.ids = [888];
exports.modules = {

/***/ 5106:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ _app)
});

// EXTERNAL MODULE: external "@emotion/react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5193);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);
;// CONCATENATED MODULE: external "next/head"
const head_namespaceObject = require("next/head");
var head_default = /*#__PURE__*/__webpack_require__.n(head_namespaceObject);
// EXTERNAL MODULE: external "@mui/material/styles"
var styles_ = __webpack_require__(8442);
;// CONCATENATED MODULE: external "@mui/material/CssBaseline"
const CssBaseline_namespaceObject = require("@mui/material/CssBaseline");
var CssBaseline_default = /*#__PURE__*/__webpack_require__.n(CssBaseline_namespaceObject);
// EXTERNAL MODULE: external "@emotion/react"
var react_ = __webpack_require__(2805);
// EXTERNAL MODULE: ./src/utils/createEmotionCache.ts
var createEmotionCache = __webpack_require__(5485);
// EXTERNAL MODULE: ./src/styles/theme.ts
var theme = __webpack_require__(483);
;// CONCATENATED MODULE: external "next/router"
const router_namespaceObject = require("next/router");
// EXTERNAL MODULE: external "next-i18next"
var external_next_i18next_ = __webpack_require__(1377);
// EXTERNAL MODULE: external "@mui/material"
var material_ = __webpack_require__(5692);
;// CONCATENATED MODULE: external "@mui/material/AppBar"
const AppBar_namespaceObject = require("@mui/material/AppBar");
var AppBar_default = /*#__PURE__*/__webpack_require__.n(AppBar_namespaceObject);
;// CONCATENATED MODULE: external "@mui/material/IconButton"
const IconButton_namespaceObject = require("@mui/material/IconButton");
var IconButton_default = /*#__PURE__*/__webpack_require__.n(IconButton_namespaceObject);
;// CONCATENATED MODULE: external "@mui/material/Toolbar"
const Toolbar_namespaceObject = require("@mui/material/Toolbar");
var Toolbar_default = /*#__PURE__*/__webpack_require__.n(Toolbar_namespaceObject);
// EXTERNAL MODULE: external "@mui/material/Container"
var Container_ = __webpack_require__(4475);
var Container_default = /*#__PURE__*/__webpack_require__.n(Container_);
;// CONCATENATED MODULE: external "@mui/material/Divider"
const Divider_namespaceObject = require("@mui/material/Divider");
var Divider_default = /*#__PURE__*/__webpack_require__.n(Divider_namespaceObject);
// EXTERNAL MODULE: external "@mui/material/Box"
var Box_ = __webpack_require__(19);
var Box_default = /*#__PURE__*/__webpack_require__.n(Box_);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
var link_default = /*#__PURE__*/__webpack_require__.n(next_link);
// EXTERNAL MODULE: ./node_modules/next/image.js
var next_image = __webpack_require__(5675);
var image_default = /*#__PURE__*/__webpack_require__.n(next_image);
;// CONCATENATED MODULE: external "@mui/icons-material/Menu"
const Menu_namespaceObject = require("@mui/icons-material/Menu");
var Menu_default = /*#__PURE__*/__webpack_require__.n(Menu_namespaceObject);
;// CONCATENATED MODULE: external "@mui/material/Drawer"
const Drawer_namespaceObject = require("@mui/material/Drawer");
var Drawer_default = /*#__PURE__*/__webpack_require__.n(Drawer_namespaceObject);
;// CONCATENATED MODULE: external "@mui/material/List"
const List_namespaceObject = require("@mui/material/List");
var List_default = /*#__PURE__*/__webpack_require__.n(List_namespaceObject);
;// CONCATENATED MODULE: ./src/components/common/AppLayout/NavBar/CustomDrawer/styles.tsx

const DrawerHeader = (0,styles_.styled)("div")(({ theme  })=>({
        display: "flex",
        alignItems: "center",
        padding: theme.spacing(0, 2),
        ...theme.mixins.toolbar,
        justifyContent: "flex-end"
    })
);

;// CONCATENATED MODULE: external "@mui/icons-material/Close"
const Close_namespaceObject = require("@mui/icons-material/Close");
var Close_default = /*#__PURE__*/__webpack_require__.n(Close_namespaceObject);
;// CONCATENATED MODULE: external "@mui/material/Button"
const Button_namespaceObject = require("@mui/material/Button");
var Button_default = /*#__PURE__*/__webpack_require__.n(Button_namespaceObject);
;// CONCATENATED MODULE: external "@mui/material/Menu"
const material_Menu_namespaceObject = require("@mui/material/Menu");
var material_Menu_default = /*#__PURE__*/__webpack_require__.n(material_Menu_namespaceObject);
// EXTERNAL MODULE: external "@mui/icons-material/ExpandMore"
var ExpandMore_ = __webpack_require__(8148);
var ExpandMore_default = /*#__PURE__*/__webpack_require__.n(ExpandMore_);
;// CONCATENATED MODULE: external "@mui/icons-material/ExpandLess"
const ExpandLess_namespaceObject = require("@mui/icons-material/ExpandLess");
var ExpandLess_default = /*#__PURE__*/__webpack_require__.n(ExpandLess_namespaceObject);
// EXTERNAL MODULE: ./src/config/index.js
var config = __webpack_require__(7182);
;// CONCATENATED MODULE: external "@mui/material/MenuItem"
const MenuItem_namespaceObject = require("@mui/material/MenuItem");
var MenuItem_default = /*#__PURE__*/__webpack_require__.n(MenuItem_namespaceObject);
// EXTERNAL MODULE: external "@mui/material/Typography"
var Typography_ = __webpack_require__(7163);
var Typography_default = /*#__PURE__*/__webpack_require__.n(Typography_);
;// CONCATENATED MODULE: ./src/components/common/AppLayout/NavBar/SelectSocialLink/SocialMenuItem/index.tsx






function SocialMenuItem(props) {
    const { handleClose , link , image , text  } = props;
    return /*#__PURE__*/ jsx_runtime_.jsx((MenuItem_default()), {
        onClick: handleClose,
        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
            target: "_blank",
            href: link,
            rel: "noopener noreferrer",
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)((Box_default()), {
                sx: {
                    display: "flex",
                    gap: 1,
                    justifyContent: "space-between",
                    alignItems: "center",
                    color: "#111A42"
                },
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx((image_default()), {
                        src: image,
                        width: 30,
                        height: 30,
                        alt: text
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx((Typography_default()), {
                        variant: "h6",
                        children: text
                    })
                ]
            })
        })
    });
}
/* harmony default export */ const SelectSocialLink_SocialMenuItem = (SocialMenuItem);

;// CONCATENATED MODULE: external "@mui/icons-material/Link"
const Link_namespaceObject = require("@mui/icons-material/Link");
var Link_default = /*#__PURE__*/__webpack_require__.n(Link_namespaceObject);
;// CONCATENATED MODULE: ./src/components/common/AppLayout/NavBar/SelectSocialLink/index.tsx







// import LinkIcon from "./LinkIcon";


const links = [
    {
        link: config/* default.communityLink.twitter */.Z.communityLink.twitter,
        image: "/image/landing/contact-twitter-icon.png",
        text: "Twitter"
    },
    {
        link: config/* default.communityLink.discord */.Z.communityLink.discord,
        image: "/image/landing/contact-discord-icon.png",
        text: "Discord"
    }, 
];
function SelectSocialLink() {
    const [anchorEl, setAnchorEl] = external_react_.useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event)=>{
        setAnchorEl(event.currentTarget);
    };
    const handleClose = external_react_.useCallback(()=>{
        setAnchorEl(null);
    }, []);
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx((Button_default()), {
                id: "language",
                "aria-controls": open ? "language" : undefined,
                "aria-haspopup": "true",
                "aria-expanded": open ? "true" : undefined,
                startIcon: /*#__PURE__*/ jsx_runtime_.jsx((Link_default()), {
                    color: "inherit"
                }),
                endIcon: open ? /*#__PURE__*/ jsx_runtime_.jsx((ExpandLess_default()), {
                    color: "inherit"
                }) : /*#__PURE__*/ jsx_runtime_.jsx((ExpandMore_default()), {
                    color: "inherit"
                }),
                size: "large",
                color: "inherit",
                onClick: handleClick,
                sx: {
                    p: 1
                }
            }),
            /*#__PURE__*/ jsx_runtime_.jsx((material_Menu_default()), {
                id: "language",
                anchorEl: anchorEl,
                open: open,
                onClose: handleClose,
                MenuListProps: {
                    "aria-labelledby": "language-button"
                },
                children: links.map((item)=>/*#__PURE__*/ jsx_runtime_.jsx(SelectSocialLink_SocialMenuItem, {
                        ...item,
                        handleClose: handleClose
                    }, item.text)
                )
            })
        ]
    });
};

;// CONCATENATED MODULE: ./src/components/common/AppLayout/NavBar/CustomDrawer/index.tsx








const CustomDrawer = ({ open , onClose  })=>{
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)((Drawer_default()), {
        anchor: "top",
        open: open,
        onClose: onClose,
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(DrawerHeader, {
                sx: {
                    bgcolor: "#1c1f28"
                },
                children: /*#__PURE__*/ jsx_runtime_.jsx((IconButton_default()), {
                    onClick: onClose,
                    children: /*#__PURE__*/ jsx_runtime_.jsx((Close_default()), {
                        sx: {
                            color: "#fff"
                        }
                    })
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx((List_default()), {
                sx: {
                    bgcolor: "#1c1f28",
                    color: "#fff"
                },
                children: /*#__PURE__*/ jsx_runtime_.jsx((Box_default()), {
                    sx: {
                        display: "flex",
                        gap: 1,
                        ml: 2
                    },
                    children: /*#__PURE__*/ jsx_runtime_.jsx(SelectSocialLink, {})
                })
            })
        ]
    });
};
/* harmony default export */ const NavBar_CustomDrawer = (CustomDrawer);

;// CONCATENATED MODULE: external "@mui/icons-material/Language"
const Language_namespaceObject = require("@mui/icons-material/Language");
var Language_default = /*#__PURE__*/__webpack_require__.n(Language_namespaceObject);
;// CONCATENATED MODULE: ./src/components/common/AppLayout/NavBar/SelectLanguage/index.tsx









function Selectlanguage() {
    const router = (0,router_namespaceObject.useRouter)();
    const [anchorEl, setAnchorEl] = external_react_.useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event)=>{
        setAnchorEl(event.currentTarget);
    };
    const handleClose = external_react_.useCallback((value)=>{
        if (value === "EN") {
            router.push("/", "/", {
                locale: "en"
            });
        } else if (value === "KO") {
            router.push("/", "/", {
                locale: "ko"
            });
        }
        setAnchorEl(null);
    }, [
        router
    ]);
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx((Button_default()), {
                id: "language",
                "aria-controls": open ? "language" : undefined,
                "aria-haspopup": "true",
                "aria-expanded": open ? "true" : undefined,
                startIcon: /*#__PURE__*/ jsx_runtime_.jsx((Language_default()), {
                    color: "inherit"
                }),
                endIcon: open ? /*#__PURE__*/ jsx_runtime_.jsx((ExpandLess_default()), {
                    color: "inherit"
                }) : /*#__PURE__*/ jsx_runtime_.jsx((ExpandMore_default()), {
                    color: "inherit"
                }),
                size: "large",
                color: "inherit",
                onClick: handleClick,
                sx: {
                    p: 1
                }
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)((material_Menu_default()), {
                id: "language",
                anchorEl: anchorEl,
                open: open,
                onClose: handleClose,
                MenuListProps: {
                    "aria-labelledby": "language-button"
                },
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx((MenuItem_default()), {
                        onClick: ()=>handleClose("EN")
                        ,
                        children: "EN"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx((MenuItem_default()), {
                        onClick: ()=>handleClose("KO")
                        ,
                        children: "KO"
                    })
                ]
            })
        ]
    });
};

;// CONCATENATED MODULE: ./src/components/common/AppLayout/NavBar/index.tsx















function ElevationScroll(props) {
    const { children  } = props;
    const trigger = (0,material_.useScrollTrigger)({
        disableHysteresis: true,
        threshold: 70,
        target:  false ? 0 : undefined
    });
    return /*#__PURE__*/ external_react_default().cloneElement(children, {
        elevation: trigger ? 4 : 0,
        color: trigger ? "secondary" : "transparent"
    });
}
function NavBar() {
    const { 0: openMenu , 1: setOpenMenu  } = (0,external_react_.useState)(false);
    const toggleMenu = (0,external_react_.useCallback)(()=>{
        setOpenMenu((state)=>!state
        );
    }, []);
    const theme = (0,material_.useTheme)();
    const match = (0,material_.useMediaQuery)(theme.breakpoints.down("xs"));
    return /*#__PURE__*/ jsx_runtime_.jsx(ElevationScroll, {
        children: /*#__PURE__*/ jsx_runtime_.jsx((AppBar_default()), {
            position: "fixed",
            sx: {
                zIndex: 99
            },
            children: /*#__PURE__*/ jsx_runtime_.jsx((Container_default()), {
                maxWidth: "lg",
                disableGutters: true,
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)((Toolbar_default()), {
                    children: [
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)((Box_default()), {
                            sx: {
                                display: "flex",
                                mx: "auto",
                                justifyContent: "space-between",
                                alignItems: "center",
                                width: "100%",
                                py: 2
                            },
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx((Box_default()), {
                                    sx: {
                                        width: {
                                            xs: 120,
                                            sm: 220
                                        },
                                        minWidth: 100,
                                        cursor: "pointer"
                                    },
                                    children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                        href: "/",
                                        as: "/",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                            children: /*#__PURE__*/ jsx_runtime_.jsx((image_default()), {
                                                src: "/image/logo.png",
                                                width: 543,
                                                height: 90,
                                                layout: "responsive",
                                                alt: "logo"
                                            })
                                        })
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx((Box_default()), {
                                    sx: {
                                        display: {
                                            xs: "none",
                                            md: "flex"
                                        },
                                        alignItems: "center"
                                    },
                                    children: /*#__PURE__*/ jsx_runtime_.jsx((Box_default()), {
                                        sx: {
                                            display: {
                                                xs: "none",
                                                md: "flex"
                                            },
                                            gap: 1,
                                            ml: 2
                                        },
                                        children: /*#__PURE__*/ jsx_runtime_.jsx(SelectSocialLink, {})
                                    })
                                })
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx((Box_default()), {
                            sx: {
                                display: {
                                    xs: "flex",
                                    md: "none"
                                }
                            },
                            children: /*#__PURE__*/ jsx_runtime_.jsx((IconButton_default()), {
                                size: "large",
                                edge: "start",
                                "aria-label": "menu",
                                onClick: toggleMenu,
                                sx: {
                                    color: "#111A42"
                                },
                                children: /*#__PURE__*/ jsx_runtime_.jsx((Menu_default()), {
                                    sx: {
                                        color: "#111A42"
                                    }
                                })
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(NavBar_CustomDrawer, {
                            open: openMenu,
                            onClose: toggleMenu
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx((Box_default()), {
                            sx: {
                                display: {
                                    xs: "none",
                                    md: "flex"
                                },
                                alignItems: "center"
                            },
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)((Box_default()), {
                                sx: {
                                    display: {
                                        xs: "none",
                                        md: "flex"
                                    },
                                    gap: 1,
                                    ml: 2
                                },
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx((Divider_default()), {
                                        orientation: "vertical",
                                        variant: "middle",
                                        flexItem: true
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx(Selectlanguage, {})
                                ]
                            })
                        })
                    ]
                })
            })
        })
    });
}
/* harmony default export */ const AppLayout_NavBar = (NavBar);

// EXTERNAL MODULE: external "@mui/icons-material"
var icons_material_ = __webpack_require__(7915);
// EXTERNAL MODULE: external "@mui/material/Grid"
var Grid_ = __webpack_require__(5612);
var Grid_default = /*#__PURE__*/__webpack_require__.n(Grid_);
;// CONCATENATED MODULE: ./src/components/view/Landing/CommunitySection/index.tsx








function CommunitySection() {
    return /*#__PURE__*/ jsx_runtime_.jsx((Container_default()), {
        maxWidth: "lg",
        sx: {
            display: "flex",
            justifyContent: "center",
            alignItems: "center"
        },
        children: /*#__PURE__*/ jsx_runtime_.jsx((Box_default()), {
            sx: {
                maxWidth: 300
            },
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)((Grid_default()), {
                container: true,
                columns: 12,
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx((Grid_default()), {
                        item: true,
                        xs: 6,
                        children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                            href: config/* default.communityLink.discord */.Z.communityLink.discord,
                            passHref: true,
                            children: /*#__PURE__*/ jsx_runtime_.jsx((Box_default()), {
                                sx: {
                                    borderRadius: "100%",
                                    background: "#fff",
                                    width: 40,
                                    cursor: "pointer",
                                    "& :hover": {
                                        opacity: 0.5
                                    }
                                },
                                children: /*#__PURE__*/ jsx_runtime_.jsx((image_default()), {
                                    src: "/image/landing/contact-discord-icon.png",
                                    layout: "responsive",
                                    width: 60,
                                    height: 60,
                                    alt: "contact-discord-icon -icon"
                                })
                            })
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx((Grid_default()), {
                        item: true,
                        xs: 6,
                        children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                            href: config/* default.communityLink.twitter */.Z.communityLink.twitter,
                            passHref: true,
                            children: /*#__PURE__*/ jsx_runtime_.jsx((Box_default()), {
                                sx: {
                                    borderRadius: "100%",
                                    background: "#fff",
                                    width: 40,
                                    cursor: "pointer",
                                    "& :hover": {
                                        opacity: 0.5
                                    }
                                },
                                children: /*#__PURE__*/ jsx_runtime_.jsx((image_default()), {
                                    src: "/image/landing/contact-twitter-icon.png",
                                    layout: "responsive",
                                    width: 60,
                                    height: 60,
                                    alt: "contact-twitter-icon"
                                })
                            })
                        })
                    })
                ]
            })
        })
    });
}
/* harmony default export */ const Landing_CommunitySection = (CommunitySection);

;// CONCATENATED MODULE: ./src/components/common/AppLayout/Footer/index.tsx








function Footer() {
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)((Container_default()), {
        disableGutters: true,
        maxWidth: false,
        sx: {
            bgcolor: "#fff"
        },
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)((Box_default()), {
                maxWidth: "lg",
                sx: {
                    display: "flex",
                    justifyContent: "space-between",
                    flexDirection: {
                        xs: "column",
                        sm: "row"
                    },
                    width: "100%",
                    px: 2,
                    mx: "auto",
                    mt: 5
                },
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx((Box_default()), {
                        sx: {
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center"
                        },
                        children: /*#__PURE__*/ jsx_runtime_.jsx((image_default()), {
                            src: "/image/logo.png",
                            width: 220,
                            height: 36,
                            alt: "NFT Towner"
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx((Box_default()), {
                        sx: {
                            mt: 5
                        },
                        children: /*#__PURE__*/ jsx_runtime_.jsx(Landing_CommunitySection, {})
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)((Box_default()), {
                maxWidth: "lg",
                sx: {
                    display: "flex",
                    justifyContent: "space-between",
                    flexDirection: {
                        xs: "column",
                        sm: "row"
                    },
                    py: 3,
                    width: "100%",
                    px: 2,
                    mx: "auto"
                },
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)((Box_default()), {
                        sx: {
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            mt: 1
                        },
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx(icons_material_.MailOutline, {}),
                            /*#__PURE__*/ jsx_runtime_.jsx((Typography_default()), {
                                variant: "body2",
                                align: "center",
                                sx: {
                                    ml: 1
                                },
                                children: "mayor@nfttown.io"
                            })
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx((Box_default()), {
                        sx: {
                            mt: 1
                        },
                        children: /*#__PURE__*/ jsx_runtime_.jsx((Typography_default()), {
                            variant: "body2",
                            align: "center",
                            children: "COPYRIGHT \u24D2 NFT Towner. ALL RIGHTS RESERVED"
                        })
                    })
                ]
            })
        ]
    });
}
/* harmony default export */ const AppLayout_Footer = (Footer);

;// CONCATENATED MODULE: ./src/components/common/AppLayout/index.tsx





function AppLayout({ children , footer , nav  }) {
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)(material_.Box, {
        sx: {
            overflowX: "hidden"
        },
        children: [
            nav && /*#__PURE__*/ jsx_runtime_.jsx(AppLayout_NavBar, {}),
            children,
            footer && /*#__PURE__*/ jsx_runtime_.jsx(AppLayout_Footer, {})
        ]
    });
}
/* harmony default export */ const common_AppLayout = (AppLayout);

;// CONCATENATED MODULE: ./src/pages/_app.tsx











// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = (0,createEmotionCache/* default */.Z)();
function MyApp(props) {
    const { Component , emotionCache =clientSideEmotionCache , pageProps  } = props;
    const router = (0,router_namespaceObject.useRouter)();
    external_react_.useEffect(()=>{}, [
        router.events
    ]);
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)(react_.CacheProvider, {
        value: emotionCache,
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)((head_default()), {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("title", {
                        children: "NFT Towner"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "viewport",
                        content: "initial-scale=1.0, width=device-width, maximum-scale=1, minimum-scale=1, user-scalable=no"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "viewport",
                        content: "width=device-width"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "viewport",
                        content: "user-scalable=0;"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        content: "yes",
                        name: "apple-mobile-web-app-capable"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        content: "minimum-scale=1.0, width=device-width, maximum-scale=1, user-scalable=no",
                        name: "viewport"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        content: "yes",
                        name: "apple-mobile-web-app-capable"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        httpEquiv: "Content-Language",
                        content: "ko,en"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "title",
                        content: "NFTtowner"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "description",
                        content: "NFT Towner\uB294 NFT \uC885\uD569 \uC815\uBCF4 \uD50C\uB7AB\uD3FC NFT Town\uC758 \uBB34\uB8CC \uBA64\uBC84\uC2ED \uC785\uB2C8\uB2E4. \uCC44\uB110\uC744 \uAD6C\uB3C5\uD558\uACE0 \uC774\uBCA4\uD2B8 \uCC38\uC5EC\uB97C \uD1B5\uD574 \uBB34\uB8CC\uB85C \uBA64\uBC84\uC2ED\uC744 \uD68D\uB4DD\uD574 \uBCF4\uC138\uC694."
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "keywords",
                        content: "NFTtowner, nfttown, NFT, tonwer, town, nft towner, nft town, \uC5D4\uC5D0\uD504\uD2F0\uD0C0\uC6B0\uB108, \uD0C0\uC6B0\uB108, \uC5D4\uC5D0\uD504\uD2F0\uD0C0\uC6B4, \uC5D4\uC5D0\uD504\uD2F0\uD0C0\uC6B4"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:type",
                        content: "website"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:title",
                        content: "NFTtowner"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:description",
                        content: "NFT Towner\uB294 NFT \uC885\uD569 \uC815\uBCF4 \uD50C\uB7AB\uD3FC NFT Town\uC758 \uBB34\uB8CC \uBA64\uBC84\uC2ED \uC785\uB2C8\uB2E4. \uCC44\uB110\uC744 \uAD6C\uB3C5\uD558\uACE0 \uC774\uBCA4\uD2B8 \uCC38\uC5EC\uB97C \uD1B5\uD574 \uBB34\uB8CC\uB85C \uBA64\uBC84\uC2ED\uC744 \uD68D\uB4DD\uD574 \uBCF4\uC138\uC694."
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:image",
                        content: "../../public/image/nfttowner_banner.png"
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(styles_.ThemeProvider, {
                theme: theme/* default */.Z,
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx((CssBaseline_default()), {}),
                    /*#__PURE__*/ jsx_runtime_.jsx(common_AppLayout, {
                        nav: true,
                        footer: true,
                        children: /*#__PURE__*/ jsx_runtime_.jsx(Component, {
                            ...pageProps
                        })
                    })
                ]
            })
        ]
    });
}
/* harmony default export */ const _app = ((0,external_next_i18next_.appWithTranslation)(MyApp));


/***/ }),

/***/ 1913:
/***/ ((module) => {

module.exports = require("@emotion/cache");

/***/ }),

/***/ 2805:
/***/ ((module) => {

module.exports = require("@emotion/react");

/***/ }),

/***/ 5193:
/***/ ((module) => {

module.exports = require("@emotion/react/jsx-runtime");

/***/ }),

/***/ 7915:
/***/ ((module) => {

module.exports = require("@mui/icons-material");

/***/ }),

/***/ 8148:
/***/ ((module) => {

module.exports = require("@mui/icons-material/ExpandMore");

/***/ }),

/***/ 5692:
/***/ ((module) => {

module.exports = require("@mui/material");

/***/ }),

/***/ 19:
/***/ ((module) => {

module.exports = require("@mui/material/Box");

/***/ }),

/***/ 4475:
/***/ ((module) => {

module.exports = require("@mui/material/Container");

/***/ }),

/***/ 5612:
/***/ ((module) => {

module.exports = require("@mui/material/Grid");

/***/ }),

/***/ 7163:
/***/ ((module) => {

module.exports = require("@mui/material/Typography");

/***/ }),

/***/ 8442:
/***/ ((module) => {

module.exports = require("@mui/material/styles");

/***/ }),

/***/ 1377:
/***/ ((module) => {

module.exports = require("next-i18next");

/***/ }),

/***/ 2796:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/head-manager-context.js");

/***/ }),

/***/ 4957:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 3539:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/detect-domain-locale.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 744:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/image-config-context.js");

/***/ }),

/***/ 5843:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/image-config.js");

/***/ }),

/***/ 8524:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4406:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/page-path/denormalize-page-path.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 3938:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/format-url.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 6689:
/***/ ((module) => {

module.exports = require("react");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [505,61,129,182], () => (__webpack_exec__(5106)));
module.exports = __webpack_exports__;

})();