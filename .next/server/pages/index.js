"use strict";
(() => {
var exports = {};
exports.id = 405;
exports.ids = [405];
exports.modules = {

/***/ 7283:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "z": () => (/* binding */ AppContainer)
/* harmony export */ });
/* harmony import */ var _mui_material_styles__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8442);
/* harmony import */ var _mui_material_styles__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_mui_material_styles__WEBPACK_IMPORTED_MODULE_0__);

const AppContainer = (0,_mui_material_styles__WEBPACK_IMPORTED_MODULE_0__.styled)("section")`
  width: 100%;
  color: #41464c;
  position: relative;
`;


/***/ }),

/***/ 1943:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ Landing_FaqSection)
});

// EXTERNAL MODULE: external "@emotion/react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5193);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "@mui/material/Container"
var Container_ = __webpack_require__(4475);
var Container_default = /*#__PURE__*/__webpack_require__.n(Container_);
// EXTERNAL MODULE: external "@mui/material/Typography"
var Typography_ = __webpack_require__(7163);
var Typography_default = /*#__PURE__*/__webpack_require__.n(Typography_);
// EXTERNAL MODULE: external "@mui/material/Box"
var Box_ = __webpack_require__(19);
var Box_default = /*#__PURE__*/__webpack_require__.n(Box_);
// EXTERNAL MODULE: external "@mui/material/styles"
var styles_ = __webpack_require__(8442);
// EXTERNAL MODULE: external "@mui/icons-material/ExpandMore"
var ExpandMore_ = __webpack_require__(8148);
var ExpandMore_default = /*#__PURE__*/__webpack_require__.n(ExpandMore_);
;// CONCATENATED MODULE: external "@mui/material/Accordion"
const Accordion_namespaceObject = require("@mui/material/Accordion");
var Accordion_default = /*#__PURE__*/__webpack_require__.n(Accordion_namespaceObject);
;// CONCATENATED MODULE: external "@mui/material/AccordionSummary"
const AccordionSummary_namespaceObject = require("@mui/material/AccordionSummary");
var AccordionSummary_default = /*#__PURE__*/__webpack_require__.n(AccordionSummary_namespaceObject);
;// CONCATENATED MODULE: external "@mui/material/AccordionDetails"
const AccordionDetails_namespaceObject = require("@mui/material/AccordionDetails");
var AccordionDetails_default = /*#__PURE__*/__webpack_require__.n(AccordionDetails_namespaceObject);
// EXTERNAL MODULE: external "next-i18next"
var external_next_i18next_ = __webpack_require__(1377);
// EXTERNAL MODULE: ./src/utils/newLineStr.ts
var newLineStr = __webpack_require__(9587);
// EXTERNAL MODULE: external "@mui/material"
var material_ = __webpack_require__(5692);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
var link_default = /*#__PURE__*/__webpack_require__.n(next_link);
;// CONCATENATED MODULE: ./src/components/view/Landing/FaqSection/Accordion/index.tsx












const Accordion = (0,styles_.styled)((props)=>/*#__PURE__*/ jsx_runtime_.jsx((Accordion_default()), {
        elevation: 0,
        square: true,
        ...props
    })
)(({ theme  })=>({
        borderBottom: `1px solid gray`,
        // backgroundColor: theme.palette.secondary.main,
        color: "#000",
        "&:last-child": {
            borderBottom: 0
        },
        "&:before": {
            display: "none"
        },
        padding: "16px 0 16px 0"
    })
);
const AccordionSummary = (0,styles_.styled)((props)=>/*#__PURE__*/ jsx_runtime_.jsx((AccordionSummary_default()), {
        expandIcon: /*#__PURE__*/ jsx_runtime_.jsx((ExpandMore_default()), {}),
        ...props
    })
)(({ theme  })=>({
        "& .MuiAccordionSummary-expandIconWrapper.Mui-expanded": {
            transform: "rotate(180deg)"
        },
        "& .MuiAccordionSummary-expandIconWrapper": {
            color: "#fff"
        }
    })
);
const AccordionDetails = (0,styles_.styled)((AccordionDetails_default()))(({ theme  })=>({
        paddingLeft: theme.spacing(4),
        bgcolor: theme.palette.secondary.main
    })
);
function CustomizedAccordions() {
    const [expanded, setExpanded] = external_react_.useState("panel0");
    const { t  } = (0,external_next_i18next_.useTranslation)();
    const handleChange = (panel)=>(event, newExpanded)=>{
            setExpanded(newExpanded ? panel : false);
        }
    ;
    return /*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
        children: t("faq", {
            returnObjects: true
        }).map((item, index)=>/*#__PURE__*/ (0,jsx_runtime_.jsxs)(Accordion, {
                expanded: expanded === "panel" + index,
                onChange: handleChange("panel" + index),
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(AccordionSummary, {
                        "aria-controls": "panel" + index + "d-content",
                        id: "panel" + index + "d-header",
                        children: /*#__PURE__*/ jsx_runtime_.jsx((Typography_default()), {
                            variant: "body1",
                            children: item.title
                        })
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)(AccordionDetails, {
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx((Typography_default()), {
                                variant: "body2",
                                children: (0,newLineStr/* newLineStr */.L)(item.description)
                            }),
                            item.link && /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                href: item.link,
                                passHref: true,
                                children: /*#__PURE__*/ jsx_runtime_.jsx(material_.Button, {
                                    children: item.linkText
                                })
                            })
                        ]
                    })
                ]
            }, item.id)
        )
    });
};

;// CONCATENATED MODULE: ./src/components/view/Landing/FaqSection/index.tsx






function FaqSection() {
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)((Container_default()), {
        disableGutters: true,
        maxWidth: "lg",
        sx: {
            py: {
                xs: 10,
                sm: 16
            },
            px: 2
        },
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx((Typography_default()), {
                variant: "h2",
                align: "center",
                sx: {
                    mt: 5
                },
                children: "FAQ"
            }),
            /*#__PURE__*/ jsx_runtime_.jsx((Box_default()), {
                sx: {
                    mt: 5
                },
                children: /*#__PURE__*/ jsx_runtime_.jsx(CustomizedAccordions, {})
            })
        ]
    });
}
/* harmony default export */ const Landing_FaqSection = (FaqSection);


/***/ }),

/***/ 9792:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5193);
/* harmony import */ var _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5675);
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_image__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _mui_material_Box__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(19);
/* harmony import */ var _mui_material_Box__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Box__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _mui_material_Container__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4475);
/* harmony import */ var _mui_material_Container__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Container__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1800);
/* harmony import */ var _mui_material_styles__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(8442);
/* harmony import */ var _mui_material_styles__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_mui_material_styles__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _mui_material_useMediaQuery__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(9868);
/* harmony import */ var _mui_material_useMediaQuery__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_mui_material_useMediaQuery__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(5692);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_mui_material__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var framer_motion__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(6197);
/* harmony import */ var src_utils_newLineStr__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(9587);
/* harmony import */ var next_i18next__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(1377);
/* harmony import */ var next_i18next__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(next_i18next__WEBPACK_IMPORTED_MODULE_11__);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([framer_motion__WEBPACK_IMPORTED_MODULE_9__]);
framer_motion__WEBPACK_IMPORTED_MODULE_9__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];












function HeroSection() {
    const theme = (0,_mui_material_styles__WEBPACK_IMPORTED_MODULE_6__.useTheme)();
    const matches = _mui_material_useMediaQuery__WEBPACK_IMPORTED_MODULE_7___default()(theme.breakpoints.down("sm"));
    const { t  } = (0,next_i18next__WEBPACK_IMPORTED_MODULE_11__.useTranslation)("common");
    return /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_styles__WEBPACK_IMPORTED_MODULE_5__/* .HeroContainer */ .OU, {
        sx: {
            maxHeight: {
                xs: 900,
                sm: 1000,
                md: 1200,
                lg: 1600
            }
        },
        children: /*#__PURE__*/ (0,_emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((_mui_material_Box__WEBPACK_IMPORTED_MODULE_3___default()), {
            sx: {
                width: "100%",
                height: "100%",
                position: "absolute"
            },
            children: [
                /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_image__WEBPACK_IMPORTED_MODULE_2___default()), {
                    src: "/image/landing/hero/hero_bg.png",
                    alt: "hero_bg",
                    layout: "fill",
                    objectFit: "cover"
                }),
                /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_material_Container__WEBPACK_IMPORTED_MODULE_4___default()), {
                    maxWidth: false,
                    sx: {
                        position: "relative",
                        height: "100%",
                        width: "100%",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        flexDirection: "row"
                    },
                    children: /*#__PURE__*/ (0,_emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_mui_material__WEBPACK_IMPORTED_MODULE_8__.Grid, {
                        container: true,
                        children: [
                            /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_8__.Grid, {
                                item: true,
                                xs: 12,
                                md: 6,
                                sx: {
                                    display: "flex",
                                    justifyContent: "flex-end",
                                    alignItems: "center"
                                },
                                children: /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_material_Box__WEBPACK_IMPORTED_MODULE_3___default()), {
                                    // sx={{ justifyContent: "center", alignItems: "center" }}
                                    component: framer_motion__WEBPACK_IMPORTED_MODULE_9__.motion.div,
                                    initial: {
                                        opacity: 0,
                                        scale: 0.9
                                    },
                                    whileInView: {
                                        opacity: 1,
                                        transition: {
                                            duration: 1
                                        }
                                    },
                                    children: matches ? /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_material_Box__WEBPACK_IMPORTED_MODULE_3___default()), {
                                        mt: 30,
                                        sx: {
                                            justifyContent: "center"
                                        },
                                        children: /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_8__.Typography, {
                                            variant: "h1",
                                            children: (0,src_utils_newLineStr__WEBPACK_IMPORTED_MODULE_10__/* .newLineStr */ .L)(t("landing_hero_title"))
                                        })
                                    }) : /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_8__.Typography, {
                                        variant: "h1",
                                        children: (0,src_utils_newLineStr__WEBPACK_IMPORTED_MODULE_10__/* .newLineStr */ .L)(t("landing_hero_title"))
                                    })
                                })
                            }),
                            /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_8__.Grid, {
                                item: true,
                                xs: 12,
                                md: 6,
                                children: /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_material_Box__WEBPACK_IMPORTED_MODULE_3___default()), {
                                    component: framer_motion__WEBPACK_IMPORTED_MODULE_9__.motion.div,
                                    animate: {
                                        y: [
                                            1000,
                                            10
                                        ]
                                    },
                                    transition: {
                                        duration: 1.5
                                    },
                                    children: /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_image__WEBPACK_IMPORTED_MODULE_2___default()), {
                                        src: "/image/landing/hero/hero_towner.png",
                                        height: 714,
                                        width: 853,
                                        alt: "ntftowner"
                                    })
                                })
                            })
                        ]
                    })
                })
            ]
        })
    });
}
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (HeroSection);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 1800:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OU": () => (/* binding */ HeroContainer)
/* harmony export */ });
/* unused harmony exports fontKeyframes, AnimationFont */
/* harmony import */ var _mui_material_styles__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8442);
/* harmony import */ var _mui_material_styles__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_mui_material_styles__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _emotion_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2805);
/* harmony import */ var _emotion_react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_emotion_react__WEBPACK_IMPORTED_MODULE_1__);


const HeroContainer = (0,_mui_material_styles__WEBPACK_IMPORTED_MODULE_0__.styled)("section")`
  overflow: hidden;
  width: 100%;
  height: 100vh;
  position: relative;
`;
const fontKeyframes = _emotion_react__WEBPACK_IMPORTED_MODULE_1__.keyframes`
  0% {
    transform: rotate(0deg) rotateY(0deg);
  }
  20% {
    transform: rotate(10deg);
  }
  40% {
    transform: rotate(-8deg) rotateY(15deg);
  }
  60%{
    transform: rotate(6deg);
  }
  80% {
    transform: rotate(-4deg);
  }
  100%{
    transform: rotate(0deg) rotateY(0deg);
  }
`;
const AnimationFont = (0,_mui_material_styles__WEBPACK_IMPORTED_MODULE_0__.styled)("div")`
  animation: ${fontKeyframes} ease 0.7s;
  animationfillmode: "forwards";
`;


/***/ }),

/***/ 8168:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ Landing_IntroductionSection)
});

// EXTERNAL MODULE: external "@emotion/react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5193);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "@mui/material/Container"
var Container_ = __webpack_require__(4475);
var Container_default = /*#__PURE__*/__webpack_require__.n(Container_);
// EXTERNAL MODULE: external "@mui/material/Box"
var Box_ = __webpack_require__(19);
var Box_default = /*#__PURE__*/__webpack_require__.n(Box_);
;// CONCATENATED MODULE: external "react-slick"
const external_react_slick_namespaceObject = require("react-slick");
var external_react_slick_default = /*#__PURE__*/__webpack_require__.n(external_react_slick_namespaceObject);
// EXTERNAL MODULE: external "@mui/material"
var material_ = __webpack_require__(5692);
// EXTERNAL MODULE: external "@mui/icons-material"
var icons_material_ = __webpack_require__(7915);
// EXTERNAL MODULE: external "@mui/material/styles"
var styles_ = __webpack_require__(8442);
;// CONCATENATED MODULE: ./src/components/view/Landing/IntroductionSection/CustomSlick/styles.tsx

const PrevButton = (0,styles_.styled)("div")`
  position: absolute;
  top: 50%;
  left: 0;
  width: 40px;
  height: 100%;
  -webkit-transform: translate(0, -50%);
  transform: translate(0, -50%);
  cursor: pointer;
  border: none;
  outline: none;
  background: transparent;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 10;
`;
const NextButton = (0,styles_.styled)("div")`
  position: absolute;
  top: 50%;
  right: 0;
  width: 40px;
  height: 100%;
  -webkit-transform: translate(0, -50%);
  transform: translate(0, -50%);
  cursor: pointer;
  border: none;
  outline: none;
  background: transparent;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 10;
`;

;// CONCATENATED MODULE: ./src/components/view/Landing/IntroductionSection/CustomSlick/index.tsx







const PrevArrow = ({ onClick , className , style  })=>{
    return /*#__PURE__*/ jsx_runtime_.jsx(PrevButton, {
        onClick: onClick,
        children: /*#__PURE__*/ jsx_runtime_.jsx(icons_material_.ArrowBackIos, {})
    });
};
const NextArrow = ({ onClick , className , style  })=>{
    return /*#__PURE__*/ jsx_runtime_.jsx(NextButton, {
        onClick: onClick,
        children: /*#__PURE__*/ jsx_runtime_.jsx(icons_material_.ArrowForwardIos, {})
    });
};
function CustomSlick(props) {
    const { children  } = props;
    const theme = (0,material_.useTheme)();
    const match = (0,material_.useMediaQuery)(theme.breakpoints.only("xs"));
    const settings = {
        infinite: true,
        slidesToShow: 5,
        // slidesToScroll: 1,
        arrows: match ? false : true,
        nextArrow: /*#__PURE__*/ jsx_runtime_.jsx(NextArrow, {}),
        prevArrow: /*#__PURE__*/ jsx_runtime_.jsx(PrevArrow, {}),
        autoplay: true,
        speed: 3500,
        autoplaySpeed: 3500,
        cssEase: "linear",
        pauseOnFocus: false,
        pauseOnHover: false,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2
                }
            }, 
        ]
    };
    return /*#__PURE__*/ jsx_runtime_.jsx((external_react_slick_default()), {
        ...settings,
        children: children
    });
}
/* harmony default export */ const IntroductionSection_CustomSlick = (CustomSlick);

// EXTERNAL MODULE: ./node_modules/next/image.js
var next_image = __webpack_require__(5675);
var image_default = /*#__PURE__*/__webpack_require__.n(next_image);
// EXTERNAL MODULE: external "next-i18next"
var external_next_i18next_ = __webpack_require__(1377);
;// CONCATENATED MODULE: ./src/components/view/Landing/IntroductionSection/SlickCard/index.tsx





function SlickCard(props) {
    const { image , title , description , impact  } = props.data;
    const { t  } = (0,external_next_i18next_.useTranslation)("common");
    const theme = (0,material_.useTheme)();
    const match = (0,material_.useMediaQuery)(theme.breakpoints.down("md"));
    return /*#__PURE__*/ jsx_runtime_.jsx((Box_default()), {
        sx: {
            width: "100%",
            px: 1
        },
        children: /*#__PURE__*/ jsx_runtime_.jsx((Box_default()), {
            sx: {
                mx: "auto"
            },
            children: /*#__PURE__*/ jsx_runtime_.jsx((image_default()), {
                src: image,
                width: 1080,
                height: 1080,
                alt: "collection",
                layout: "responsive",
                priority: true
            })
        })
    });
}
/* harmony default export */ const IntroductionSection_SlickCard = (SlickCard);

// EXTERNAL MODULE: ./src/utils/newLineStr.ts
var newLineStr = __webpack_require__(9587);
;// CONCATENATED MODULE: ./src/components/view/Landing/IntroductionSection/index.tsx










function IntroductionSection() {
    const { t  } = (0,external_next_i18next_.useTranslation)("common");
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)((Container_default()), {
        disableGutters: true,
        maxWidth: false,
        sx: {
            py: 10
        },
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)((Container_default()), {
                sx: {
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    flexDirection: "column",
                    mb: 5
                },
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(material_.Typography, {
                        variant: "h2",
                        align: "center",
                        sx: {
                            mt: 5
                        },
                        children: (0,newLineStr/* newLineStr */.L)(t("landing_NFT_title"))
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(material_.Typography, {
                        variant: "body1",
                        align: "center",
                        sx: {
                            mt: 5
                        },
                        children: (0,newLineStr/* newLineStr */.L)(t("landing_nft_description"))
                    })
                ]
            }),
            /*#__PURE__*/ jsx_runtime_.jsx((Container_default()), {
                disableGutters: true,
                maxWidth: false,
                sx: {
                    // bgcolor: "#D1E6F0",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    flexDirection: "column",
                    py: 5
                },
                children: /*#__PURE__*/ jsx_runtime_.jsx((Container_default()), {
                    disableGutters: true,
                    maxWidth: false,
                    children: /*#__PURE__*/ jsx_runtime_.jsx(IntroductionSection_CustomSlick, {
                        children: t("landing_intro_pfp", {
                            returnObjects: true
                        }).map((item, index)=>/*#__PURE__*/ jsx_runtime_.jsx(IntroductionSection_SlickCard, {
                                data: item
                            }, index)
                        )
                    })
                })
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)((Box_default()), {
                sx: {
                    mt: 4,
                    position: "relative",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center"
                },
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(material_.Button, {
                        variant: "contained",
                        size: "small",
                        sx: {
                            borderRadius: 3,
                            mr: 10,
                            width: {
                                xs: 130,
                                sm: 150
                            },
                            fontWeight: 600,
                            py: 1,
                            bgcolor: "#2976bc",
                            textAlign: "center",
                            justifyContent: "center"
                        },
                        onClick: ()=>{
                            alert(t("lading_alter_after_minting"));
                        },
                        children: "Open Sea"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(material_.Button, {
                        variant: "contained",
                        size: "small",
                        sx: {
                            borderRadius: 3,
                            width: {
                                xs: 130,
                                sm: 150
                            },
                            fontWeight: 600,
                            py: 1,
                            bgcolor: "#2976bc",
                            justifyContent: "center",
                            alignItems: "center",
                            position: "relative"
                        },
                        onClick: ()=>{
                            alert(t("lading_alter_after_minting"));
                        },
                        children: "rarity chart"
                    })
                ]
            })
        ]
    });
}
/* harmony default export */ const Landing_IntroductionSection = (IntroductionSection);


/***/ }),

/***/ 1687:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5193);
/* harmony import */ var _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _mui_material_Grid__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5612);
/* harmony import */ var _mui_material_Grid__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Grid__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _mui_material_Typography__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(7163);
/* harmony import */ var _mui_material_Typography__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Typography__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _mui_material_Box__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(19);
/* harmony import */ var _mui_material_Box__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Box__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5675);
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_image__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var next_i18next__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1377);
/* harmony import */ var next_i18next__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_i18next__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _mui_material_Container__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(4475);
/* harmony import */ var _mui_material_Container__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_mui_material_Container__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var src_utils_newLineStr__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(9587);
/* harmony import */ var framer_motion__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(6197);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(5692);
/* harmony import */ var _mui_material__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_mui_material__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var src_config__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(7182);
/* harmony import */ var _mui_material_useMediaQuery__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(9868);
/* harmony import */ var _mui_material_useMediaQuery__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_mui_material_useMediaQuery__WEBPACK_IMPORTED_MODULE_12__);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([framer_motion__WEBPACK_IMPORTED_MODULE_9__]);
framer_motion__WEBPACK_IMPORTED_MODULE_9__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];













function StorySection() {
    const { t  } = (0,next_i18next__WEBPACK_IMPORTED_MODULE_6__.useTranslation)("common");
    const theme = (0,_mui_material__WEBPACK_IMPORTED_MODULE_10__.useTheme)();
    const matches = _mui_material_useMediaQuery__WEBPACK_IMPORTED_MODULE_12___default()(theme.breakpoints.up("md"));
    return /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_material_Container__WEBPACK_IMPORTED_MODULE_7___default()), {
        disableGutters: true,
        maxWidth: "lg",
        sx: {
            py: {
                xs: 10,
                sm: 16
            },
            px: 2
        },
        children: /*#__PURE__*/ (0,_emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((_mui_material_Grid__WEBPACK_IMPORTED_MODULE_2___default()), {
            container: true,
            children: [
                /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_material_Grid__WEBPACK_IMPORTED_MODULE_2___default()), {
                    item: true,
                    xs: 12,
                    md: 6,
                    children: /*#__PURE__*/ (0,_emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((_mui_material_Box__WEBPACK_IMPORTED_MODULE_4___default()), {
                        sx: {
                            display: "flex",
                            flexDirection: "column",
                            justifyContent: "center",
                            // alignItems: "flex-start",
                            height: "100%"
                        },
                        children: [
                            /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_material_Box__WEBPACK_IMPORTED_MODULE_4___default()), {
                                sx: {
                                    mb: 6
                                },
                                component: framer_motion__WEBPACK_IMPORTED_MODULE_9__.motion.div,
                                initial: {
                                    opacity: 0
                                },
                                whileInView: {
                                    opacity: 1,
                                    transition: {
                                        duration: 1
                                    }
                                },
                                children: /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_material_Typography__WEBPACK_IMPORTED_MODULE_3___default()), {
                                    variant: "h2",
                                    children: (0,src_utils_newLineStr__WEBPACK_IMPORTED_MODULE_8__/* .newLineStr */ .L)(t("landing_story_title"))
                                })
                            }),
                            /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_material_Box__WEBPACK_IMPORTED_MODULE_4___default()), {
                                component: framer_motion__WEBPACK_IMPORTED_MODULE_9__.motion.div,
                                initial: {
                                    opacity: 0
                                },
                                whileInView: {
                                    opacity: 1,
                                    transition: {
                                        duration: 1,
                                        delay: 0.2
                                    }
                                },
                                children: /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_material_Typography__WEBPACK_IMPORTED_MODULE_3___default()), {
                                    variant: "body1",
                                    sx: {
                                        mb: 3
                                    },
                                    children: (0,src_utils_newLineStr__WEBPACK_IMPORTED_MODULE_8__/* .newLineStr */ .L)(t("landing_story_detail"))
                                })
                            }),
                            /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_material_Box__WEBPACK_IMPORTED_MODULE_4___default()), {
                                sx: {
                                    display: "flex",
                                    justifyContent: {
                                        xs: "center",
                                        md: "flex-start"
                                    }
                                },
                                children: /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_material_Box__WEBPACK_IMPORTED_MODULE_4___default()), {
                                    component: framer_motion__WEBPACK_IMPORTED_MODULE_9__.motion.div,
                                    initial: {
                                        opacity: 0
                                    },
                                    whileInView: {
                                        opacity: 1,
                                        transition: {
                                            duration: 1,
                                            delay: 0.5
                                        }
                                    },
                                    children: /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_mui_material__WEBPACK_IMPORTED_MODULE_10__.Button, {
                                        variant: "contained",
                                        size: "small",
                                        sx: {
                                            borderRadius: 20,
                                            width: 150,
                                            p: 1.5,
                                            bgcolor: "#C9DEF2",
                                            color: "#313131",
                                            "&:hover": {
                                                backgroundColor: "#325F80",
                                                color: "#fff"
                                            },
                                            mt: 3
                                        },
                                        onClick: ()=>window.open(src_config__WEBPACK_IMPORTED_MODULE_11__/* ["default"].communityLink.nfttown */ .Z.communityLink.nfttown, "_blank")
                                        ,
                                        children: /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_material_Typography__WEBPACK_IMPORTED_MODULE_3___default()), {
                                            sx: {
                                                fontWeight: 600
                                            },
                                            children: "NFT Town"
                                        })
                                    })
                                })
                            })
                        ]
                    })
                }),
                /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_material_Grid__WEBPACK_IMPORTED_MODULE_2___default()), {
                    item: true,
                    xs: 12,
                    md: 6,
                    children: /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((_mui_material_Box__WEBPACK_IMPORTED_MODULE_4___default()), {
                        component: framer_motion__WEBPACK_IMPORTED_MODULE_9__.motion.div,
                        animate: {
                            x: [
                                500,
                                10
                            ]
                        },
                        transition: {
                            duration: 1.5
                        },
                        pt: 10,
                        children: /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_image__WEBPACK_IMPORTED_MODULE_5___default()), {
                            src: "/image/landing/story/story_towner.png",
                            width: 632,
                            height: 816,
                            alt: "story"
                        })
                    })
                })
            ]
        })
    });
}
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (StorySection);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 7135:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5193);
/* harmony import */ var _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var src_components_common_GlobalStyled__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7283);
/* harmony import */ var _FaqSection__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1943);
/* harmony import */ var _HeroSection__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9792);
/* harmony import */ var _IntroductionSection__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(8168);
/* harmony import */ var _StorySection__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1687);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_HeroSection__WEBPACK_IMPORTED_MODULE_3__, _StorySection__WEBPACK_IMPORTED_MODULE_5__]);
([_HeroSection__WEBPACK_IMPORTED_MODULE_3__, _StorySection__WEBPACK_IMPORTED_MODULE_5__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);






function Landing() {
    return /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: /*#__PURE__*/ (0,_emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(src_components_common_GlobalStyled__WEBPACK_IMPORTED_MODULE_1__/* .AppContainer */ .z, {
            children: [
                /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_HeroSection__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z, {}),
                /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_StorySection__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {}),
                /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_IntroductionSection__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z, {}),
                /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_FaqSection__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {})
            ]
        })
    });
}
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Landing);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 5970:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__),
/* harmony export */   "getStaticProps": () => (/* binding */ getStaticProps)
/* harmony export */ });
/* harmony import */ var _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5193);
/* harmony import */ var _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_i18next_serverSideTranslations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5460);
/* harmony import */ var next_i18next_serverSideTranslations__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_i18next_serverSideTranslations__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var src_components_view_Landing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7135);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([src_components_view_Landing__WEBPACK_IMPORTED_MODULE_2__]);
src_components_view_Landing__WEBPACK_IMPORTED_MODULE_2__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];



const Home = ()=>{
    return /*#__PURE__*/ _emotion_react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(src_components_view_Landing__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {});
};
async function getStaticProps(ctx) {
    return {
        props: {
            ...await (0,next_i18next_serverSideTranslations__WEBPACK_IMPORTED_MODULE_1__.serverSideTranslations)(ctx.locale, [
                "common"
            ])
        }
    };
}
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Home);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 9587:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "L": () => (/* binding */ newLineStr)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

function newLineStr(html) {
    if (!html) return null;
    const lines = html.split("\n");
    if (lines.length === 0) {
        return html;
    }
    return lines.map((line, index)=>react__WEBPACK_IMPORTED_MODULE_0___default().createElement("span", {
            key: index
        }, line, react__WEBPACK_IMPORTED_MODULE_0___default().createElement("br"))
    );
}


/***/ }),

/***/ 2805:
/***/ ((module) => {

module.exports = require("@emotion/react");

/***/ }),

/***/ 5193:
/***/ ((module) => {

module.exports = require("@emotion/react/jsx-runtime");

/***/ }),

/***/ 7915:
/***/ ((module) => {

module.exports = require("@mui/icons-material");

/***/ }),

/***/ 8148:
/***/ ((module) => {

module.exports = require("@mui/icons-material/ExpandMore");

/***/ }),

/***/ 5692:
/***/ ((module) => {

module.exports = require("@mui/material");

/***/ }),

/***/ 19:
/***/ ((module) => {

module.exports = require("@mui/material/Box");

/***/ }),

/***/ 4475:
/***/ ((module) => {

module.exports = require("@mui/material/Container");

/***/ }),

/***/ 5612:
/***/ ((module) => {

module.exports = require("@mui/material/Grid");

/***/ }),

/***/ 7163:
/***/ ((module) => {

module.exports = require("@mui/material/Typography");

/***/ }),

/***/ 8442:
/***/ ((module) => {

module.exports = require("@mui/material/styles");

/***/ }),

/***/ 9868:
/***/ ((module) => {

module.exports = require("@mui/material/useMediaQuery");

/***/ }),

/***/ 1377:
/***/ ((module) => {

module.exports = require("next-i18next");

/***/ }),

/***/ 5460:
/***/ ((module) => {

module.exports = require("next-i18next/serverSideTranslations");

/***/ }),

/***/ 2796:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/head-manager-context.js");

/***/ }),

/***/ 4957:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 3539:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/detect-domain-locale.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 744:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/image-config-context.js");

/***/ }),

/***/ 5843:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/image-config.js");

/***/ }),

/***/ 8524:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4406:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/page-path/denormalize-page-path.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 3938:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/format-url.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 6689:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 6197:
/***/ ((module) => {

module.exports = import("framer-motion");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [505,61,182], () => (__webpack_exec__(5970)));
module.exports = __webpack_exports__;

})();