module.exports = {
  apps: [
    {
      name: "NFTtowner",
      script: "node_modules/next/dist/bin/next",
      args: "start",
      cwd: "./",
      watch: false,
      instances: 1, // 0으로 설정시 CPU 코어 수 만큼 프로세스 생성
      log_date_format: "YYYY-MM-DD HH:mm Z",
      exec_mode: "cluster",
      env: {
        PORT: 3000,
        NODE_ENV: "development",
      },
      env_production: {
        PORT: 8080,
        NODE_ENV: "production",
      },
      css: { extract: false },
    },
  ],
};
